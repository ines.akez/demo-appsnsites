import { Component, OnInit } from '@angular/core';
import {Product} from '../model/product';


@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  title: string;
  productList: Product[];
  maxPrice: number;
  hideForm: boolean;
  constructor() { }

  ngOnInit(): void {
    this.title = 'E-commerce';
    this.hideForm = true;
    this.productList = [
      {id: 1, title : 'T-shirt 1', price: 20 , quantity: 10},
      {id: 2, title : 'T-shirt 2', price: 10 , quantity: 0},
      {id: 3, title : 'T-shirt 3', price: 100 , quantity: 0}
    ]
  }
  
  pushProduct(p: Product){
    this.productList.push(p);
    this.hideForm = true;
  }
  showForm(){
    this.hideForm = false;
  }

 

}
