import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {ListProductComponent} from './list-product/list-product.component';
import {TodoListComponent} from './todo-list/todo-list.component';
import {FormProductComponent} from './form-product/form-product.component';
import { LoginComponent } from './login/login.component';

const ROUTES: Routes  = [
  {path: 'products', component: ListProductComponent},
  {path: 'todos', component: TodoListComponent},
  {path:  'login',component:  LoginComponent},
  {path: 'addProduct', component: FormProductComponent},
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(ROUTES)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
