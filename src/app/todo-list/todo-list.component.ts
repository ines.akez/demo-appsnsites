import {Component, OnInit} from '@angular/core';
import {Todo} from '../model/todo';

import {TodoService} from '../services/todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  listTodo: Todo[];
 


  constructor( private todoS: TodoService) {
  }

  ngOnInit(): void {
    this.todoS.getTodos().subscribe((data: Todo[]) => this.listTodo = data);
  }

  

}
