import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Todo} from '../model/todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: HttpClient) {
  }

  getTodos() {
    return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos');
  }

  getTodoUser(id: number) {
    return this.http.get<Todo[]>('//jsonplaceholder.typicode.com/todos?userId=' + id);
  }

  deleteTodo(id) {
    let url = '';
    this.http.delete('url' + id);
  }
}
