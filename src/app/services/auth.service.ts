
import { AngularFireAuth } from  "@angular/fire/auth";
import { Injectable } from '@angular/core';
import * as firebase from 'firebase'; 
import { Router } from '@angular/router';
@Injectable()
export class AuthService {
  

  constructor(private afAuth: AngularFireAuth,private router:Router ) { 
    
  }

  login() {

    
    this.afAuth.signInWithRedirect(new firebase.default.auth.GoogleAuthProvider());
    this.router.navigate(['index']);
  }

  logout() { 
    this.afAuth.signOut();
  }

}
